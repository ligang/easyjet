#ifndef BBTTANALYSIS_CHANNELS
#define BBTTANALYSIS_CHANNELS

namespace HHBBTT
{

  enum Channel
  {
    LepHad = 0,
    HadHad = 1,
    HadHad1B = 2,
    ZCR = 3,
    TopEMuCR = 4,
  };

}

#endif
